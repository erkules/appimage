FROM registry.gitlab.com/erkules/baseimage:IMAGETAG
ARG COMMIT_USER=whoot
ARG CI_COMMIT_SHA=whoot
LABEL BASEIMAGE=registry.gitlab.com/erkules/baseimage:IMAGETAG
LABEL COMMIT_USER=${COMMIT_USER}
LABEL CI_COMMIT_SHA=${CI_COMMIT_SHA}
RUN apk update && apk add curl
COPY Dockerfile /Dockerfile
